<?php

/**
 * 
 * Template Name: Test Portal
 * 
*/

get_header(); ?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.css">
<link rel="stylesheet" href="http://192.168.1.141/total_sms/wp-content/themes/thegem/css/icons-material.css">


<style>
    .vc_custom_1471350571268 {
        margin-bottom: 0px !important;
        padding-top: 40px !important;
        padding-bottom: 60px !important;
        background-color: #eb5353 !important;
    }
    .vc_custom_1539681267584 {
        margin-bottom: 0px !important;
        background-image: url(http://192.168.1.141/total_sms/wp-content/uploads/2018/10/banner-total-sms2-.jpg?id=31253) !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }
    .vc_custom_1539690855767 {
        background-image: url(http://192.168.1.141/total_sms/wp-content/uploads/2018/10/sms-bg-image.jpg?id=31252) !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }
    .flag-icon {
        height: 30px;
        width: 30px;
        float: left;
    }
    .flag-title {
        text-align: center;
    }
    .dataTables_filter {
        float: right;
    }
    .pagination {
        display: inline-flex;
        float: right;
        list-style-type: none;
    }
    #testlist td {
        text-align: center;
    }
    #testdata th, #testdata td {
        padding: 16px;
    }
</style>


<div id="main-content" class="main-content">
    
    
    <div id="page-title" class="page-title-block page-title-alignment-center page-title-style-1 has-background-image" style="background-image: url(http://192.168.1.141/total_sms/wp-content/uploads/2018/10/test-banner.jpg);padding-top: 130px;padding-bottom: 130px;">
        <div class="container">
            <div class="page-title-title" style="">
                <h1 style="">  Test Portal</h1>
            </div>
        </div>
        <div class="breadcrumbs-container">
            <div class="container">
                <div class="breadcrumbs">
                    <span>
                        <a href="http://192.168.1.141/total_sms/" itemprop="url">
                            <span itemprop="title">Home</span>
                        </a>    
                    </span>
                    <span class="divider">
                        <span class="bc-devider">     
                        </span> 
                    </span> 
                    <span class="current">Test Portal</span>
                </div><!-- .breadcrumbs -->
            </div>
        </div>
    </div>

    
<div class="block-content">
    <div class="container">
        
        <div class="panel row">

			<div class="panel-center col-xs-12">
				<article id="post-31277" class="post-31277 page type-page status-publish hentry">

					<div class="entry-content post-content">


<div id="vc_row-5bc728ec46f0a" class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="clearboth"></div><div class="gem-divider " style="margin-top: 40px;"></div></div></div></div></div>

<div id="vc_row-5bc728ec4743d" class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="clearboth"></div><div class="gem-divider " style="margin-top: 40px;"></div>
                <div class="vc_btn3-container vc_btn3-center">
                    <img class="loading-image" src="<?php echo get_site_url();?>/wp-content/uploads/flags/loader-blue.gif" style="height:50px;display:none;"/>
                    <button class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-square vc_btn3-style-3d vc_btn3-icon-left vc_btn3-color-pink" onclick="testportalList()"><i class="vc_btn3-icon fa fa-refresh"></i> SYNCRONIZE</button>
                </div>
<div class="clearboth"></div><div class="gem-divider" style="margin-top: 40px;"></div></div></div></div></div>


<div id="vc_row-5bc728ec47b46" class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gem-table gem-table-responsive gem-table-style-1 row-headers">
<table id="testdata" style="width: 100%;">
<thead>
<tr>
<th style="background-color: #ffde97;">
<h6>DATE</h6>
</th>
<th style="background-color: #e4f4a5;">
<h6>DDI</h6>
</th>
<th style="background-color: #a3e7f0;">
<h6>CLI</h6>
</th>
<th style="background-color: #a8aaf4;">
<h6>BODY</h6>
</th>
<th style="background-color: #ffde97;">
<h6>STATUS</h6>
</th>
</tr>
</thead>
<tbody id="testlist">
<!--<tr>
<td style="text-align: center;">Lorem ipsum dolor sit amet</td>
<td style="text-align: center;">Consectetur adipisicing</td>
<td style="text-align: center;">Lorem ipsum dolor sit amet</td>
<td style="text-align: center;">Consectetur adipisicing</td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>
<tr>
<td style="text-align: center;">Consectetur adipisicing</td>
<td style="text-align: center;">Elit sed do eiusmod</td>
<td style="text-align: center;">Consectetur adipisicing</td>
<td style="text-align: center;">Elit sed do eiusmod</td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>
<tr>
<td style="text-align: center;">Elit sed do eiusmod</td>
<td style="text-align: center;"><span style="color: #ff0000;">Tempor incididunt labore *</span></td>
<td style="text-align: center;">Elit sed do eiusmod</td>
<td style="text-align: center;">Tempor incididunt ut labore</td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>
<tr>
<td style="text-align: center;">Tempor incididunt ut labore</td>
<td style="text-align: center;">Et dolore magna aliqua</td>
<td style="text-align: center;">Tempor incididunt ut labore</td>
<td style="text-align: center;">Et dolore magna aliqua</td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>
<tr>
<td style="text-align: center;">Et dolore magna aliqua</td>
<td style="text-align: center;">Ut enim ad minim</td>
<td style="text-align: center;">Et dolore magna aliqua</td>
<td style="text-align: center;">Ut enim ad minim</td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>
<tr>
<td style="text-align: center;">Ut enim ad minim</td>
<td style="text-align: center;">Veniam, quis nostrud</td>
<td style="text-align: center;">Ut enim ad minim</td>
<td style="text-align: center;"><span style="color: #ff0000;">Veniam, quis nostrud *</span></td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>
<tr>
<td style="text-align: center;">Veniam, quis nostrud</td>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Veniam, quis nostrud</td>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>
<tr>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Nisi ut aliquip ex ea</td>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Nisi ut aliquip ex ea</td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>
<tr>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Nisi ut aliquip ex ea</td>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Nisi ut aliquip ex ea</td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>
<tr>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Nisi ut aliquip ex ea</td>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Nisi ut aliquip ex ea</td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>
<tr>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Nisi ut aliquip ex ea</td>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Nisi ut aliquip ex ea</td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>
<tr>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Nisi ut aliquip ex ea</td>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Nisi ut aliquip ex ea</td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>
<tr>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Nisi ut aliquip ex ea</td>
<td style="text-align: center;">Exercitation ullamco laboris</td>
<td style="text-align: center;">Nisi ut aliquip ex ea</td>
<td style="text-align: center;">Consectetur adipisicing</td>
</tr>-->
</tbody>
</table>
</div></div></div></div></div>


<div id="vc_row-5bc728ec4834d" class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="clearboth"></div><div class="gem-divider " style="margin-top: 10px;"></div>
                <div class="vc_btn3-container vc_btn3-center">
                    <img class="loading-image" src="<?php echo get_site_url();?>/wp-content/uploads/flags/loader-blue.gif" style="height:50px;display:none;"/>
                    <button class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-square vc_btn3-style-3d vc_btn3-icon-left vc_btn3-color-pink" onclick="testportalList()"><i class="vc_btn3-icon fa fa-refresh"></i> SYNCRONIZE</button>
                </div>
<div class="clearboth"></div><div class="gem-divider " style="margin-top: 40px;"></div></div></div></div></div>


<div id="vc_row-5bc728ec489b2" class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="clearboth"></div><div class="gem-divider " style="margin-top: 40px;"></div></div></div></div></div>


<div id="vc_row-5bc728ec48d46" class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gem-table gem-table-responsive gem-table-style-3"><table style="width: 100%;">
<thead>
<tr>
<th>
<h6><img class="alignnone size-full wp-image-4282" style="margin-top: -3px;" src="<?php echo get_site_url();?>/wp-content/uploads/flags/flag-ic.png" width="20" height="22">&nbsp; Country</h6>
</th>
<th>
<h6><img class="alignnone size-full wp-image-4283" style="margin-top: -3px;" src="<?php echo get_site_url();?>/wp-content/uploads/flags/phone-ic.png" width="22" height="22">&nbsp; Number</h6>
</th>
</tr>
</thead>
<tbody>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_Belgium.png" alt="Flag_of_Belgium"><span class="flag-title">&nbsp; BELGIUM - 2</span></td>
    <td style="text-align: center;">+32467401120</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_Belgium.png" alt="Flag_of_Belgium"><span class="flag-title">&nbsp; BELGIUM - 8</span></td>
    <td style="text-align: center;">+32468900928</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_Cameroon.png" alt="Flag_of_Cameroon"><span class="flag-title">&nbsp; CAMEROON - 1</span></td>
    <td style="text-align: center;">+23722945509</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_the_Democratic_Republic_of_the_Congo.png" alt="Flag_of_the_Democratic_Republic_of_the_Congo"><span class="flag-title">&nbsp; CONGO - 11</span></td>
    <td style="text-align: center;">+2431230560</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_estonia.png" alt="Flag_of_estonia"><span class="flag-title">&nbsp; ESTONIA - 2</span></td>
    <td style="text-align: center;">+37258990380</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_fui.png" alt="Flag_of_fui"><span class="flag-title">&nbsp; FUI - WHITE</span></td>
    <td style="text-align: center;">+6995198100</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_Guinea.png" alt="Flag_of_Guinea"><span class="flag-title">&nbsp; GUINEA GAMMA - WHITE</span></td>
    <td style="text-align: center;">+224791112510</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/logo.png" alt="logo"><span class="flag-title">&nbsp; LITHUANIA - D</span></td>
    <td style="text-align: center;">+37066541130</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_malawi.png" alt="Flag_of_malawi"><span class="flag-title">&nbsp; MALAWI - 1</span></td>
    <td style="text-align: center;">+265212341281</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_nauru.png" alt="Flag_of_nauru"><span class="flag-title">&nbsp; NAURU - BLACK</span></td>
    <td style="text-align: center;">+6745597000</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_Papua_New_Guinea.png" alt="Flag_of_Papua_New_Guinea"><span class="flag-title">&nbsp; PAPUA NEW GUINEA - WHITE</span></td>
    <td style="text-align: center;">+67570896600</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_Papua_New_Guinea.png" alt="Flag_of_Papua_New_Guinea"><span class="flag-title">&nbsp; PAPUA NEW GUINEA - WHITE</span></td>
    <td style="text-align: center;">+67570896800</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/logo.png" alt="logo"><span class="flag-title">&nbsp; POLAND - A</span></td>
    <td style="text-align: center;">+48780203020</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/logo.png" alt="logo"><span class="flag-title">&nbsp; RUSSIA - 3</span></td>
    <td style="text-align: center;">+79585389450</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_russia.png" alt="Flag_of_russia"><span class="flag-title">&nbsp; RUSSIA - 5</span></td>
    <td style="text-align: center;">+79697531590</td>
</tr>
</tbody>
</table>
</div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gem-table gem-table-responsive gem-table-style-3"><table style="width: 100%;">
<thead>
<tr>
<th>
<h6><img class="alignnone size-full wp-image-4282" style="margin-top: -3px;" src="<?php echo get_site_url();?>/wp-content/uploads/flags/flag-ic.png" width="20" height="22">&nbsp; Country</h6>
</th>
<th>
<h6><img class="alignnone size-full wp-image-4283" style="margin-top: -3px;" src="<?php echo get_site_url();?>/wp-content/uploads/flags/phone-ic.png" width="22" height="22">&nbsp; Number</h6>
</th>
</tr>
</thead>
<tbody>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Nexus-Earth-Satellite2.png" alt="Nexus-Earth-Satellite2"><span class="flag-title">&nbsp; SATELLITE - WHITE</span></td>
    <td style="text-align: center;">+8818460019</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/red-plus.png" alt="red-plus"><span class="flag-title">&nbsp; SWITZERLAND - 1</span></td>
    <td style="text-align: center;">+41799770484</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/red-plus.png" alt="red-plus"><span class="flag-title">&nbsp; SWITZERLAND - 3</span></td>
    <td style="text-align: center;">+41799770671</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_tonga.png" alt="Flag_of_tonga"><span class="flag-title">&nbsp; TONGA - WHITE</span></td>
    <td style="text-align: center;">+6768482000</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_the_United_Kingdom.png" alt="Flag_of_the_United_Kingdom"><span class="flag-title">&nbsp; UK - 1</span></td>
    <td style="text-align: center;">+447924767900</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_the_United_Kingdom.png" alt="Flag_of_the_United_Kingdom"><span class="flag-title">&nbsp; UK - A</span></td>
    <td style="text-align: center;">+447452221100</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_the_United_Kingdom.png" alt="Flag_of_the_United_Kingdom"><span class="flag-title">&nbsp; UK - B</span></td>
    <td style="text-align: center;">+447452300100</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_the_United_Kingdom.png" alt="Flag_of_the_United_Kingdom"><span class="flag-title">&nbsp; UK - C</span></td>
    <td style="text-align: center;">+447452317840</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_the_United_Kingdom.png" alt="Flag_of_the_United_Kingdom"><span class="flag-title">&nbsp; UK - D</span></td>
    <td style="text-align: center;">+447624126100</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_the_United_Kingdom.png" alt="Flag_of_the_United_Kingdom"><span class="flag-title">&nbsp; UK - G</span></td>
    <td style="text-align: center;">+447537606100</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_the_United_Kingdom.png" alt="Flag_of_the_United_Kingdom"><span class="flag-title">&nbsp; UK - H</span></td>
    <td style="text-align: center;">+447441007200</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_the_United_Kingdom.png" alt="Flag_of_the_United_Kingdom"><span class="flag-title">&nbsp; UK - I</span></td>
    <td style="text-align: center;">+447924800500</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_the_United_Kingdom.png" alt="Flag_of_the_United_Kingdom"><span class="flag-title">&nbsp; UK - J</span></td>
    <td style="text-align: center;">+447418495100</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_the_United_Kingdom.png" alt="Flag_of_the_United_Kingdom"><span class="flag-title">&nbsp; UK - K</span></td>
    <td style="text-align: center;">+447924767900</td>
</tr>
<tr>
    <td><img class="flag-icon" src="<?php echo get_site_url();?>/wp-content/uploads/flags/Flag_of_the_United_Kingdom.png" alt="Flag_of_the_United_Kingdom"><span class="flag-title">&nbsp; UK - L</span></td>
    <td style="text-align: center;">+447924767900</td>
</tr>
</tbody>
</table>
</div></div></div></div></div>


<div id="vc_row-5bc728ec4984d" class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_empty_space" style="height: 40px"><span class="vc_empty_space_inner"></span></div>
</div></div></div></div>


<div id="vc_row-5bc728ec49ce0" class="vc_row wpb_row vc_row-fluid custom-icon"><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gem-textbox"><div class="gem-textbox-inner" style="border-top-left-radius: 57px;border-top-right-radius: 57px;border-bottom-right-radius: 57px;"><div class="gem-textbox-content" style="background-color: #f0f3f2;background-position: center top;padding-top: 20px;padding-bottom: 60px;padding-left: 70px;padding-right: 0px;"><div class="gem-icon-with-text gem-icon-with-text-icon-size-medium gem-icon-with-text-float-right"><div style="margin-bottom:0px;margin-top:0px; " class="gem-icon-with-text-icon"><div class="gem-icon gem-icon-pack-material gem-icon-size-medium  gem-icon-shape-circle" style="border-color: #348aa7;opacity: 1;"><div class="gem-icon-inner" style="background-color: #348aa7;">
		<span class="gem-icon-half-1" style="color: #3c3950;"><span class="back-angle"></span></span>
		<span class="gem-icon-half-2" style="color: #3c3950;"><span class="back-angle"></span></span></div></div></div><div class="gem-icon-with-text-content"><div class="gem-icon-with-text-text"><div class="clearboth"></div><div class="gem-divider " style="margin-top: 25px;"></div>
                <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                                <h4><span class="light">Lorem ipsum dolor</span></h4>
                        </div>
                </div>

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>

		</div>
	</div>
</div></div><div class="clearboth"></div></div></div></div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gem-textbox"><div class="gem-textbox-inner" style="border: 1px solid #dfe5e8;border-top-right-radius: 57px;border-bottom-right-radius: 57px;border-bottom-left-radius: 57px;"><div class="gem-textbox-content" style="background-position: center top;padding-top: 20px;padding-bottom: 60px;padding-left: 70px;padding-right: 0px;"><div class="gem-icon-with-text gem-icon-with-text-icon-size-medium gem-icon-with-text-float-right"><div style="margin-bottom:0px;margin-top:0px; " class="gem-icon-with-text-icon"><div class="gem-icon gem-icon-pack-material gem-icon-size-medium  gem-icon-shape-circle" style="border-color: #612f4b;opacity: 1;"><div class="gem-icon-inner" style="background-color: #612f4b;">
		<span class="gem-icon-half-1" style="color: #ffffff;"><span class="back-angle"></span></span>
		<span class="gem-icon-half-2" style="color: #ffffff;"><span class="back-angle"></span></span></div></div></div><div class="gem-icon-with-text-content"><div class="gem-icon-with-text-text"><div class="clearboth"></div><div class="gem-divider " style="margin-top: 25px;"></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h4><span class="light">Lorem ipsum dolor</span></h4>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>

		</div>
	</div>
</div></div><div class="clearboth"></div></div></div></div></div></div></div></div></div>


<div id="vc_row-5bc728ec4ab8d" class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_empty_space" style="height: 40px"><span class="vc_empty_space_inner"></span></div>
</div></div></div></div>


<div class="vc_row-full-width-before"></div>


<div id="vc_row-5bc728ec4affd" data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1471350571268 vc_row-has-fill vc_row-o-equal-height vc_row-o-content-middle vc_row-flex" style="position: relative; left: -37px; box-sizing: border-box; width: 1286px; padding-left: 37px; padding-right: 37px;"><script type="text/javascript">if (typeof(gem_fix_fullwidth_position) == "function") { gem_fix_fullwidth_position(document.getElementById("vc_row-5bc728ec4affd")); }</script><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-12 vc_col-xs-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="clearboth"></div><div class="gem-divider " style="margin-top: 3px;"></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h5 style="text-align: center;"><span class="light" style="position: relative; width: 330px; color: #ffffff;">EARN EXTRA CASH BY RECOMMENDING TOTAL SMS</span></h5>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-5 vc_col-md-12 vc_col-xs-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="title-xlarge" style="text-align: center;"><span style="color: #ffffff; font-size: 70px; letter-spacing: 1px;">REFERRAL</span></div>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-12 vc_col-xs-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="clearboth"></div><div class="gem-divider " style="margin-top: 15px;"></div><div class="gem-button-container gem-button-position-center lazy-loading  lazy-loading-end-animation"><a class="gem-button gem-button-size-medium gem-button-style-outline gem-button-text-weight-thin gem-button-border-1 lazy-loading-item lazy-loading-item-drop-right" data-ll-effect="drop-right-without-wrap" style="border-radius: 0px;border-color: #ffffff;color: #ffffff;" onmouseleave="this.style.borderColor='#ffffff';this.style.backgroundColor='transparent';this.style.color='#ffffff';" onmouseenter="this.style.borderColor='#ffffff';this.style.backgroundColor='#ffffff';this.style.color='#eb5353';" href="#" target="_self">LEARN MORE</a></div> </div></div></div></div>


<div class="vc_row-full-width vc_clearfix"></div>


<div class="vc_row-full-width-before"></div>


<div id="vc_row-5bc728ec4ba75" data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1539681267584 vc_row-has-fill" style="position: relative; left: -37px; box-sizing: border-box; width: 1286px; padding-left: 37px; padding-right: 37px;"><script type="text/javascript">if (typeof(gem_fix_fullwidth_position) == "function") { gem_fix_fullwidth_position(document.getElementById("vc_row-5bc728ec4ba75")); }</script><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="clearboth"></div><div class="gem-divider " style="margin-top: 100px;"></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="title-h2" style="color: #3c3950;"><span class="title-xlarge" style="color: #e12545;">HOT</span> NEWS</div>
<div class="title-h6" style="color: #3c3950;">HOT NEWS brings you the latest special promotions, deal of the week, access information and newest terminations for our Standard SMS. To always be kept in the loop simply sign up for our newsletter here.</div>

		</div>
	</div>
<div class="clearboth"></div><div class="gem-divider " style="margin-top: 50px;"></div><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gem-icon gem-icon-pack-material gem-icon-size-small  gem-icon-shape-circle" style="border-color: #242332;opacity: 1;"><div class="gem-icon-inner" style="background-color: #242332;">
		<span class="gem-icon-half-1" style="color: #ffffff;"><span class="back-angle"></span></span>
		<span class="gem-icon-half-2" style="color: #ffffff;"><span class="back-angle"></span></span></div></div><div class="clearboth"></div><div class="gem-divider " style="margin-top: 30px;"></div>
	<div class="wpb_text_column wpb_content_element  vc_custom_1539597441401">
		<div class="wpb_wrapper">
			<div class="title-h5"><span style="color: #e12545;">TERMINATION OF THE WEEK:</span></div>

		</div>
	</div>
<div class="clearboth"></div><div class="gem-divider " style="margin-top: 15px;"></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p><b>Papua New Guinea&nbsp;</b>please test to&nbsp;<b>67570896100</b> now and check your results&nbsp;<a href="#">here</a></p>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="gem-icon gem-icon-pack-material gem-icon-size-small  gem-icon-shape-circle" style="border-color: #242332;opacity: 1;"><div class="gem-icon-inner" style="background-color: #242332;">
		<span class="gem-icon-half-1" style="color: #ffffff;"><span class="back-angle"></span></span>
		<span class="gem-icon-half-2" style="color: #ffffff;"><span class="back-angle"></span></span></div></div><div class="clearboth"></div><div class="gem-divider " style="margin-top: 30px;"></div>
	<div class="wpb_text_column wpb_content_element  vc_custom_1539597455783">
		<div class="wpb_wrapper">
			<div class="title-h5"><span style="color: #e12545;">DEAL OF THE MONTH</span></div>

		</div>
	</div>
<div class="clearboth"></div><div class="gem-divider " style="margin-top: 15px;"></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p><b>Estonia&nbsp;</b>&amp;&nbsp;<b>Russia&nbsp;</b>– NOW OFFERING INCREASED PAYOUT, PLEASE TEST AND CONTACT US WHEN SUCCESSFUL.</p>

		</div>
	</div>
</div></div></div></div><div class="clearboth"></div><div class="gem-divider " style="margin-top: 100px;"></div></div></div></div></div>


<div class="vc_row-full-width vc_clearfix"></div>


<div class="vc_row-full-width-before"></div>


<div id="vc_row-5bc728ec4ccf9" data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1539690855767 vc_row-has-fill" style="position: relative; left: -37px; box-sizing: border-box; width: 1286px; padding-left: 37px; padding-right: 37px;"><script type="text/javascript">if (typeof(gem_fix_fullwidth_position) == "function") { gem_fix_fullwidth_position(document.getElementById("vc_row-5bc728ec4ccf9")); }</script><div class="counters_title wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div data-number-format="(ddd).ddd" class="gem-counter-box row inline-row inline-row-center gem-counter-style-2 lazy-loading lazy-loading-not-hide lazy-loading-before-start-animation" data-ll-item-delay="0"><div class="gem-counter col-md-3 col-sm-4 col-xs-6 inline-column gem-counter-effect-simple lazy-loading-item lazy-loading-item-action" data-hover-icon-color="rgba(16,17,26,0.5)"><div class="gem-counter-inner"><div class="gem-counter-icon"><div class="gem-counter-icon-circle-1" style="border-color: #abed81;"><div class="gem-counter-icon-circle-2" style="border-color: #abed81;"><div class="gem-icon gem-icon-pack-material gem-icon-size-medium  gem-icon-shape-circle gem-simple-icon lazy-loading-item lazy-loading-item-fading" style="opacity: 1;"><div class="gem-icon-inner" style="">
		<span class="gem-icon-half-1" style="color: #abed81;"><span class="back-angle"></span></span>
		<span class="gem-icon-half-2" style="color: #abed81;"><span class="back-angle"></span></span></div></div></div></div></div><div class="gem-counter-number" style="color: #abed81"><div class="gem-counter-odometer" data-to="50">0</div><span class="gem-counter-suffix">%</span></div><div class="gem-counter-text styled-subtitle lazy-loading-item lazy-loading-item-fading" style="color: #ffffff">SSMS</div><a class="gem-counter-link" href="#" target="_self"></a></div></div><div class="gem-counter col-md-3 col-sm-4 col-xs-6 inline-column gem-counter-effect-simple lazy-loading-item lazy-loading-item-action" data-hover-icon-color="rgba(16,17,26,0.5)"><div class="gem-counter-inner"><div class="gem-counter-icon"><div class="gem-counter-icon-circle-1" style="border-color: #00d4a0;"><div class="gem-counter-icon-circle-2" style="border-color: #00d4a0;"><div class="gem-icon gem-icon-pack-material gem-icon-size-medium  gem-icon-shape-circle gem-simple-icon lazy-loading-item lazy-loading-item-fading" style="opacity: 1;"><div class="gem-icon-inner" style="">
		<span class="gem-icon-half-1" style="color: #00d4a0;"><span class="back-angle"></span></span>
		<span class="gem-icon-half-2" style="color: #00d4a0;"><span class="back-angle"></span></span></div></div></div></div></div><div class="gem-counter-number" style="color: #00d4a0"><div class="gem-counter-odometer" data-to="30">0</div><span class="gem-counter-suffix">%</span></div><div class="gem-counter-text styled-subtitle lazy-loading-item lazy-loading-item-fading" style="color: #ffffff">PSMS</div><a class="gem-counter-link" href="#" target="_self"></a></div></div><div class="gem-counter col-md-3 col-sm-4 col-xs-6 inline-column gem-counter-effect-simple lazy-loading-item lazy-loading-item-action" data-hover-icon-color="rgba(16,17,26,0.5)"><div class="gem-counter-inner"><div class="gem-counter-icon"><div class="gem-counter-icon-circle-1" style="border-color: #00bcd4;"><div class="gem-counter-icon-circle-2" style="border-color: #00bcd4;"><div class="gem-icon gem-icon-pack-material gem-icon-size-medium  gem-icon-shape-circle gem-simple-icon lazy-loading-item lazy-loading-item-fading" style="opacity: 1;"><div class="gem-icon-inner" style="">
		<span class="gem-icon-half-1" style="color: #00bcd4;"><span class="back-angle"></span></span>
		<span class="gem-icon-half-2" style="color: #00bcd4;"><span class="back-angle"></span></span></div></div></div></div></div><div class="gem-counter-number" style="color: #00bcd4"><div class="gem-counter-odometer" data-to="5">0</div><span class="gem-counter-suffix">%</span></div><div class="gem-counter-text styled-subtitle lazy-loading-item lazy-loading-item-fading" style="color: #ffffff">OTHERS</div><a class="gem-counter-link" href="#" target="_self"></a></div></div><div class="gem-counter col-md-3 col-sm-4 col-xs-6 inline-column gem-counter-effect-simple lazy-loading-item lazy-loading-item-action" data-hover-icon-color="rgba(16,17,26,0.5)"><div class="gem-counter-inner"><div class="gem-counter-icon"><div class="gem-counter-icon-circle-1" style="border-color: #ff9069;"><div class="gem-counter-icon-circle-2" style="border-color: #ff9069;"><div class="gem-icon gem-icon-pack-material gem-icon-size-medium  gem-icon-shape-circle gem-simple-icon lazy-loading-item lazy-loading-item-fading" style="opacity: 1;"><div class="gem-icon-inner" style="">
		<span class="gem-icon-half-1" style="color: #ff9069;"><span class="back-angle"></span></span>
		<span class="gem-icon-half-2" style="color: #ff9069;"><span class="back-angle"></span></span></div></div></div></div></div><div class="gem-counter-number" style="color: #ff9069"><div class="gem-counter-odometer" data-to="5">0</div><span class="gem-counter-suffix">%</span></div><div class="gem-counter-text styled-subtitle lazy-loading-item lazy-loading-item-fading" style="color: #ffffff">OTHERS</div><a class="gem-counter-link" href="#" target="_self"></a></div></div></div></div></div></div></div>
                
                
<div class="vc_row-full-width vc_clearfix"></div>

                                        </div>
                                </article>
                                    
                        </div>
            
                </div>

        </div>
    </div>
    
    
</div>

<script>
    
    $(document).ready(function() {
        $('#testdata').DataTable({
            "sorting": false
        });
        testportalList();
    } );

</script>

<script>  
    
    function testportalList() {
        
             $.ajax({
                        url: "http://192.168.1.141/total_sms/get_sms_data.php",
                        //dataType: 'json'
                        beforeSend: function() {
                            $(".loading-image").show();
                         },
                        success:function(result){
                            //alert(result);
                            //console.log(result.abc);
                            var decoded_data = JSON.parse(result);
                            
                            var html_data;
                            //alert(decoded_data.length);
                            for(var i=0;i<decoded_data.length;i++)
                            {
                                //var id = decoded_data[0].id;
                                var start_stamp = decoded_data[i].start_stamp;
                                var destination_addr = decoded_data[i].destination_addr;
                                //var source_addr = decoded_data[i].source_addr;
                                //var static_dt = '1234567890';
                                //var phone = decoded_data[i].source_addr.substring(0, 7)+'XXX';
                                //var phone = static_dt.substring(0, 7)+'XXX';
                                //var source_addr = phone;
                                
                                var cntstr = decoded_data[i].source_addr.length;
                                var str = '';
                                if(cntstr > 7)
                                {
                                    var str = 'XXX';
                                }
                                var phone = decoded_data[i].source_addr.substring(0, 7)+str;
                                var source_addr = phone;
                                
                                var short_message = decoded_data[i].short_message;
                                var status = 'UNPAID';
                                html_data += "<tr><td>"+start_stamp+"</td><td>"+destination_addr+"</td><td>"+source_addr+"</td><td>"+short_message+"</td><td>"+status+"</td></tr>";
                            }
                            $("#testlist").html("");
                            $("#testlist").html(html_data);
                            $(".loading-image").hide();
                        },
                        error: function (error) {
                            $(".loading-image").hide();
                        }
                       
                     });
        
            //e.preventDefault();
//            $.ajax({
//                      //alert();
//                    url: "http://api.telcoitlimited.com/SmsInboundAllocationSearch?authkey=87527533-927b-11e8-872b-509a4c6568ec&PartitionSmsInboundAllocationSearch[date_range]=2018-10-05+00:00+-+2018-10-17+17:17",
//                    type: "GET",
//                    dataType: 'json',
//                    async: true,
//                    username: 'testsms',
//                    password: 'test123',
//                    //data: '{ "comment" }',
//                    success: function(data){
//                      alert();
//                        var decoded_data = json.parse(data);
//
//                        var html_data;
//                        for(var i=0;i<decoded_data.length;i++)
//                        {
//                            var id = decoded_data[0].id;
//                            var start_stamp = decoded_data[0].start_stamp;
//                            html_data += "<tr><td>"+start_stamp+"</td><td>"+start_stamp+"</td><td>"+start_stamp+"</td><td>"+start_stamp+"</td><td>"+start_stamp+"</td></tr>";
//                        }
//                        $("#testlist").html("");
//                        $("#testlist").html(html_data);
//                    
//                    }
//                    
//             });
             //e.preventDefault();
//             $.ajax({
//                url: "http://api.telcoitlimited.com/SmsInboundAllocationSearch?authkey=87527533-927b-11e8-872b-509a4c6568ec&PartitionSmsInboundAllocationSearch[date_range]=2018-10-05+00:00+-+2018-10-17+17:17",
//                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//                beforeSend: function(xhr) {
//                  xhr.setRequestHeader("Authorization", "Basic " + btoa("testsms:test123"));
//                },
//                //method: 'GET',
//                //type: 'GET',
//                method: 'GET',
//                dataType: 'json',
//                //crossOrigin: false,
//                //
//                //processData: true,
//                //headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
//                success: function (data) {
//                  alert(JSON.stringify(data));
//                },
//                error: function(){
//                  alert("Cannot get data");
//                }
//             });

//                $.ajax({
//                    url: "http://api.telcoitlimited.com/SmsInboundAllocationSearch?authkey=87527533-927b-11e8-872b-509a4c6568ec&PartitionSmsInboundAllocationSearch[date_range]=2018-10-05+00:00+-+2018-10-06+17:17",
//                  beforeSend: function(xhr) {
//    xhr.setRequestHeader("Authorization", "Basic " + btoa("testsms:test123"));
//  },
//                   headers: {‘Content-Type’: ‘application/x-www-form-urlencoded’},
//                   success: function (data) {
//                    alert(JSON.stringify(data));
//                  },
//                  error: function(){
//                    alert("Cannot get data");
//                  }
//                });

                   
        }
        
</script>

<?php
get_footer();
?>